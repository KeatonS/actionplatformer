TODO:
- Make certain animations continue from in air to on ground (through different layers)
- Fix shooting with no ammo to stop the animation
-
COMBAT RULES:
- Hitting enemies increases *Berserk Meter*
- Berserk meter slowly decreases when not attacking.
- Three main attack types: 
	- Physical (done with main weapon like sword or spear. Increases meter.)
	- Projectile (performed with guns, can be reflected/defelcted by physical attacks. Consumes meter.)
	- Grab (performed with the grappling hook. Can be blocked with physical or projectile. Used to close gaps. No effect on meter.)
- Use this meter for special attacks
	- Firearms consume this meter per shot, many kinds of firearms
	- Hold a "Special Button" and hit attack to perform an attack that regains player health
	- Once filled to a certain amount, enter Berserk Mode by pressing multiple face buttons at once.
		- Initiated by a large blast all around the player which pushes back enemies, can be done while in hitstun as a combo breaker.
		- In Berserk Mode, the berserk meter now acts as a timer showing how long it lasts.
		- Player health constantly goes down, but all Physical attacks are in Special mode which regains health.
		- Main idea is to competely overwhelm the opponent/enemy in order to stay alive.

- Gameplay Mechanics
	-- Stand
	-- Walk
	-- Dash - GorA
	-- Jump
	-- Attack -  GorA
	-- Shoot - GorA
	-- WallSlide
	-- WallJump
