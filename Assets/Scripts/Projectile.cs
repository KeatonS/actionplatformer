﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]

public class Projectile : MonoBehaviour {

    public AnimationCurve xAxisCurve;
    public AnimationCurve yAxisCurve;

    public float speed;
    public float lifetime;
    int side;

    Rigidbody2D myRB;
    float timer = 0.0f;
    float normalTime;
    float resolution;
    private Vector2 parentVeloc = new Vector2();

    void Start()
    {
        myRB = GetComponent<Rigidbody2D>();
        GameObject.Destroy(this.gameObject, lifetime);
        resolution = 1 / lifetime;
        side = (int) Mathf.Sign(transform.rotation.y / -180);
    }

    void Update()
    {
        normalTime = (timer / lifetime);

        myRB.velocity = new Vector2(xAxisCurve.Evaluate(normalTime) * (speed + Mathf.Abs(parentVeloc.x)) * side, yAxisCurve.Evaluate(normalTime) * speed);

        timer += Time.deltaTime * resolution * speed;
    }
    

    public void SetStats(Vector3 parentVeloc, int side)
    {
        //this.parentVeloc = parentVeloc;
        this.side = side;
    }
}
