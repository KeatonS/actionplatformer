﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State {

    protected GameObject owner;
    protected Rigidbody2D rigidbody;
    protected PlayerStats stats;
    protected PlayerAnimatorParamManager animParams;
    //protected Animator animator;

    public State(GameObject owner)
    {
        this.owner = owner;
        rigidbody = owner.GetComponent<Rigidbody2D>();
        stats = owner.GetComponent<PlayerStats>();
        //animator = owner.GetComponent<Animator>();
        animParams = owner.GetComponent<PlayerAnimatorParamManager>();
    }
    public virtual void Execute() { }
    public virtual void Enter() { }
    public virtual void Exit() { }
}

public class TakeDamageState : State
{
    public TakeDamageState(GameObject owner) : base(owner) { hitHandler = owner.GetComponent<PlayerHitHandler>(); }
    private Vector2 knockBack = new Vector2(0f,0f);
    private float damage = 0f;
    private PlayerHitHandler hitHandler;
    private HitBoxData hitBoxData;

    bool knockBackRecieved = false;
    public override void Enter()
    {
        base.Enter();
        hitBoxData = hitHandler.GetBestHit();
        damage = hitHandler.GetDamage();
        knockBack = hitHandler.GetKnockBack();
        // hitHandler.HitStop(hitBoxData.hitstopTime);
        knockBackRecieved = false;
    }
    public override void Execute()
    {
        if (!hitHandler.inHitStop && !knockBackRecieved)
        {
            rigidbody.velocity = knockBack;
            knockBackRecieved = true;
        }

        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
        knockBackRecieved = false;

    }
}

public class DeathState : State
{
    public DeathState(GameObject owner) : base(owner) { }

    public override void Enter()
    {
        base.Enter();
        animParams.SetCanRotate(false);
    }
    public override void Execute()
    {
        base.Execute();
        if(animParams.GetGrounded()) rigidbody.simulated = false;
        rigidbody.velocity = new Vector2(0f, rigidbody.velocity.y);
    }
    public override void Exit()
    {
        base.Exit();
        animParams.SetCanRotate(true);
        rigidbody.simulated = true;
    }
}


// ------------ MOVEMENT STATES --------------- //
public abstract class MovementState : State
{
    public MovementState(GameObject owner) : base(owner) { animator = owner.GetComponent<Animator>(); }
    protected bool attackAble = false;
    protected bool shootAble = false;
    protected bool eightWayAble = false;
    protected Animator animator;
    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute() /// Still keeping references to animator for this
    {
        base.Execute();
        if (attackAble && animator.GetBool("Attack") && NoActiveLayers())
        {
            animParams.SetLayerWeight(1, 1);
        }
        if (shootAble && animator.GetBool("Shoot") && NoActiveLayers())
        {        
            animParams.SetLayerWeight(2, 1);
        }
        if (eightWayAble && animator.GetBool("Special") && NoActiveLayers())
        {
            animParams.SetLayerWeight(3, 1);
        }
    }
    public override void Exit()
    {
        base.Exit();
    }
    private bool NoActiveLayers()
    {
        return animator.GetLayerWeight(1) == 0 && animator.GetLayerWeight(2) == 0 && animator.GetLayerWeight(3) == 0;
    }
    private void UnsetAllLayers()
    {
        animParams.SetLayerWeight(1, 0);
        animParams.SetLayerWeight(2, 0);
        animParams.SetLayerWeight(3, 0);
    }
}

public class WallSlideState : MovementState
{
    public WallSlideState(GameObject owner) : base(owner) { shootAble = true; }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
        rigidbody.velocity = new Vector2(0f, rigidbody.velocity.y / stats.wallFriction);
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class SwingState : MovementState
{
    public SwingState(GameObject owner) : base(owner) { grapplingHook = owner.GetComponent<GrapplingHook>(); }

    protected GrapplingHook grapplingHook;

    public override void Enter()
    {
        base.Enter();

        rigidbody.gravityScale *= stats.swingGravMultiplier;
    }
    public override void Execute()
    {
        grapplingHook.Execute();
        base.Execute();

        // Player adjusting self in midair
        if (animator.GetFloat("Horizontal_Input") != 0f)
        {
            if (Mathf.Abs(rigidbody.velocity.x) <= stats.maxAirSpeedX || Mathf.Sign(rigidbody.velocity.x) != Mathf.Sign(animator.GetFloat("Horizontal_Input")))
            {
                rigidbody.velocity += Vector2.right * (stats.swingAcceleration * animator.GetFloat("Horizontal_Input")) * Time.deltaTime;
            }
        }

    }
    public override void Exit()
    {
        grapplingHook.Exit();
        base.Exit();

        rigidbody.gravityScale = 1f;
        //TODO Exit velocity multiplier
        if(rigidbody.velocity.y > 0)
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, rigidbody.velocity.y * stats.swingExitVelocity);
    }
}
// ------------ GROUND MOVEMENT STATES --------------- //
public abstract class GroundMovementState : MovementState
{
    public GroundMovementState(GameObject owner) : base(owner) { }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class IdleState : GroundMovementState
{
    public IdleState(GameObject owner) : base(owner) { attackAble = true; shootAble = true; eightWayAble = true; }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();

        if (animator.GetBool("Grounded") && Mathf.Abs(rigidbody.velocity.x) >= Mathf.Epsilon)
        {
            rigidbody.velocity = Vector2.Lerp(rigidbody.velocity, new Vector2(0, rigidbody.velocity.y), stats.groundFriction * Time.fixedDeltaTime); // Friction
        }
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class WalkState : GroundMovementState
{
    public WalkState(GameObject owner) : base(owner) { attackAble = true; shootAble = true; eightWayAble = true; }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
        rigidbody.velocity = new Vector2(stats.walkSpeed * animator.GetFloat("Horizontal_Input"), rigidbody.velocity.y);
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class DashState : GroundMovementState
{
    public DashState(GameObject owner) : base(owner) { attackAble = true; shootAble = true; }

    public override void Enter()
    {
        base.Enter();
        rigidbody.gravityScale = 0f;
        animParams.SetCanRotate(false);
    }
    public override void Execute()
    {
        base.Execute();
        rigidbody.velocity = new Vector2(stats.dashSpeed * animParams.GetSideFacing(), 0f);
    }
    public override void Exit()
    {
        base.Exit();
        rigidbody.gravityScale = 1f;
        animParams.SetCanRotate(true);
    }
}

// ------------ AIR MOVEMENT STATES --------------- //
public abstract class AirMovementState : MovementState
{
    public AirMovementState(GameObject owner) : base(owner) { }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();


        // Player adjusting self in midair
        if (animator.GetFloat("Horizontal_Input") != 0f)
        {
            if (Mathf.Abs(rigidbody.velocity.x) <= stats.maxAirSpeedX || Mathf.Sign(rigidbody.velocity.x) != Mathf.Sign(animator.GetFloat("Horizontal_Input")))
            {
                rigidbody.velocity += Vector2.right * (stats.airAcceleration * animator.GetFloat("Horizontal_Input")) * Time.deltaTime;
            }
        }

        // Maintain Terminal fall speed
        if (rigidbody.velocity.y < stats.maxAirSpeedY)
        {
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, stats.maxAirSpeedY);
        }
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class JumpState : AirMovementState
{
    public JumpState(GameObject owner) : base(owner) { attackAble = true; shootAble = true; eightWayAble = true; }

    public override void Enter()
    {
        base.Enter();

        if (animator.GetBool("Grounded"))
        {
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, stats.jumpVelocity);
        }
    }
    public override void Execute()
    {
        base.Execute();
        // Increase Gravity after reaching jump peak
        if (rigidbody.velocity.y > 0 && !animator.GetBool("JumpHold"))// + Y veloc, going up and no jump input
        {
            rigidbody.velocity += Vector2.up * Physics.gravity.y * (stats.lowJumpMultiplier - 1) * Time.deltaTime; // Add gravity multiplier
        }

    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class FallState : AirMovementState
{
    public FallState(GameObject owner) : base(owner) { attackAble = true; shootAble = true; eightWayAble = true; }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
        if (rigidbody.velocity.y < 0 && rigidbody.velocity.y > stats.maxAirSpeedY) // - Y veloc, going down
        {
            rigidbody.velocity += Vector2.up * Physics.gravity.y * (stats.normalJumpMultiplier - 1) * Time.deltaTime; // Add gravity multiplier
        }
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class WallJumpState : AirMovementState
{
    public WallJumpState(GameObject owner) : base(owner) { }

    public override void Enter()
    {
        base.Enter();
        rigidbody.velocity = new Vector2(stats.wallJumpVelocityX * animParams.GetSideFacing() * -1, stats.wallJumpVelocityY);
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

// ------------ ATTACK STATES --------------- //
public abstract class AttackState : State
{
    public List<HitBoxData> attackHitData;
    public Attack attack;
    protected Transform hitBoxContainer;
    protected MovementState moveState;
    protected bool canRotate = false;

    public AttackState(GameObject owner, Attack attack) : base(owner)
    {
        moveState = new IdleState(owner);
        this.attack = attack;
        attackHitData = attack.attackData;
        hitBoxContainer = owner.transform.GetChild(2);    
    }

    public override void Enter()
    {
        base.Enter();
        moveState.Enter();
        animParams.SetCanRotate(canRotate);

        for(int i = 0; i < attackHitData.Count; i++)
        {
            hitBoxContainer.GetChild(i).GetComponent<StandardHitBox>().myData = attackHitData[i];
            hitBoxContainer.GetChild(i).GetComponent<StandardHitBox>().specialProperty = attack.GetComponent<SpecialAttackProperty>();
        }
    }
    public override void Execute()
    {
        base.Execute();
        moveState.Execute();
    }
    public override void Exit()
    {
        base.Exit();
        if(!animParams.holdingAttack)animParams.SetLayerWeight(1, 0);
        moveState.Exit();
        animParams.SetCanRotate(true);
    }
}

public class CanAttackState : AttackState
{
    public CanAttackState(GameObject owner, Attack attack) : base(owner, attack) { moveState = new IdleState(owner); }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        animParams.SetLayerWeight(1, 0);
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class StandAttackState : AttackState
{
    public StandAttackState(GameObject owner, Attack attack) : base(owner, attack) { moveState = new IdleState(owner); }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class AirAttackState : AttackState
{
    public AirAttackState(GameObject owner, Attack attack) : base(owner, attack) { moveState = new JumpState(owner); }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class DashAttackState : AttackState
{
    public DashAttackState(GameObject owner, Attack attack) : base(owner, attack) { moveState = new DashState(owner); }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}


// ------------ SHOOT STATES --------------- //
public abstract class ShootState : State
{
    protected MovementState moveState;
    protected bool canRotate = true;
    protected Firearm currentGun;

    public ShootState(GameObject owner) : base(owner)
    {
        moveState = new IdleState(owner);
        currentGun = ((owner.GetComponent<PlayerStateMachineDictionary>()).firearm).GetComponent<Firearm>();
    }

    public override void Enter()
    {
        base.Enter();
        moveState.Enter();
        animParams.SetCanRotate(canRotate);
    }
    public override void Execute()
    {
        base.Execute();
        moveState.Execute();
        if (currentGun.canStopFiring )
        {
            if(!animParams.GetShootInput())animParams.SetLayerWeight(2, 0);
        }
    }
    public override void Exit()
    {
        base.Exit();
        animParams.SetLayerWeight(2, 0);
        moveState.Exit();
        animParams.SetCanRotate(true);
    }
}

public class StandShootState : ShootState
{
    public StandShootState(GameObject owner) : base(owner) { moveState = new IdleState(owner); }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class FallShootState : ShootState
{
    public FallShootState(GameObject owner) : base(owner) { moveState = new FallState(owner); }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class JumpShootState : ShootState
{
    public JumpShootState(GameObject owner) : base(owner) { moveState = new JumpState(owner); }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class DashShootState : ShootState
{
    public DashShootState(GameObject owner) : base(owner) { moveState = new DashState(owner); }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class WalkShootState : ShootState
{
    public WalkShootState(GameObject owner) : base(owner) { moveState = new WalkState(owner); }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class WallSlideShootState : ShootState
{
    public WallSlideShootState(GameObject owner) : base(owner) { moveState = new WallSlideState(owner); }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class WallJumpShootState : ShootState
{
    public WallJumpShootState(GameObject owner) : base(owner) { moveState = new WallJumpState(owner); }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

// ------------ 8-WAY STATES --------------- //
public abstract class EightWayState : State
{
    protected bool canRotate = false;
    protected GrapplingHook grapplingHook;

    public EightWayState(GameObject owner) : base(owner)
    { grapplingHook = owner.GetComponent<GrapplingHook>();  }

    public override void Enter()
    {
        animParams.SetCanRotate(canRotate);
        grapplingHook.Enter();
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        animParams.SetCanRotate(true);
        base.Exit();
        animParams.SetLayerWeight(3, 0);
    }
}

public class AimState : EightWayState
{
    public AimState(GameObject owner) : base(owner)
    {
    }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
        //animator.SetLayerWeight(2, 0);
    }
}

public class EightWayShootState : EightWayState
{
    protected MovementState parallelState;

    public EightWayShootState(GameObject owner) : base(owner)
    {    }

    public override void Enter()
    {
        parallelState.Enter();
        base.Enter();
    }
    public override void Execute()
    {
        parallelState.Execute();
        base.Execute();
    }
    public override void Exit()
    {
        parallelState.Exit();
        base.Exit();
    }
}

public class EightWayAirShootState : EightWayShootState
{
    public EightWayAirShootState(GameObject owner) : base(owner)
    { parallelState = new FallState(owner); }

    public override void Enter()
    {   
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class EightWayStandShootState : EightWayShootState
{
    public EightWayStandShootState(GameObject owner) : base(owner)
    { parallelState = new IdleState(owner); }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

