﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// HitBoxes for projectile Attacks
public class ProjectileHitBox : HitBox {

    public bool destroyOnHit = true;
    Rigidbody2D myRB;
    
    void Start()
    {
        myData.owner = transform.root.gameObject;
        myRB = GetComponent<Rigidbody2D>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        myData.owner = transform.root.gameObject;
        int side = (int)Mathf.Sign(myRB.velocity.x);

        if (col.gameObject == myData.owner)
        {
            return;
        }
        else if ((enemyLayers & 1 << col.gameObject.layer) == 1 << col.gameObject.layer)//enemyLayers == col.gameObject.layer)
        {
            col.gameObject.GetComponent<PlayerHitHandler>().NotifyOnHit(myData, side, null);
            if (destroyOnHit) PerformOnAnyCollision();
        }
        else if ((wallLayers & 1 << col.gameObject.layer) == 1 << col.gameObject.layer)//wallLayers == col.gameObject.layer)
        {
            PerformOnAnyCollision();
        }
    }

    void PerformOnAnyCollision()
    {
        this.GetComponent<Animator>().SetTrigger("Hit");
        this.GetComponent<Projectile>().speed = 0f;
        Destroy(this.gameObject, 0.1f);
    }
}
