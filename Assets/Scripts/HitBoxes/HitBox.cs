﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider2D))]
public class HitBox : MonoBehaviour
{
    public HitBoxData myData;
    public LayerMask wallLayers;
    public LayerMask enemyLayers;

    protected int entityLayer = 11;
    protected string enemyTag = "Enemy";
    protected string playerTag = "Player";

}
