﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// HitBoxes for normal attacks
public class StandardHitBox : HitBox {
    [HideInInspector]public SpecialAttackProperty specialProperty;

    void Start()
    {
        myData.owner = transform.root.gameObject;
        this.transform.localScale = Vector3.zero;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        myData.owner = transform.root.gameObject;
        int side = (int)Mathf.Sign(col.transform.position.x - myData.owner.transform.position.x);

        if (col.gameObject == myData.owner)
        {
            return;
        }
        else if ((enemyLayers & 1 << col.gameObject.layer) == 1 << col.gameObject.layer)//enemyLayers == col.gameObject.layer)
        {
            col.gameObject.GetComponent<PlayerHitHandler>().NotifyOnHit(myData, side, specialProperty);
            myData.owner.GetComponent<PlayerHitHandler>().NotifyOnAttack(myData, specialProperty);
            //specialProperty.OnHit();
        }
        else if ((wallLayers & 1 << col.gameObject.layer) == 1 << col.gameObject.layer)//wallLayers == col.gameObject.layer)
        {
            //Debug.Log("Hit a wall!!!");
        }
    }
    void OnDisable()
    {
        this.transform.localScale = Vector3.zero;
    }
    void OnEnable()
    {
        this.transform.localScale = Vector3.one;
    }
}
