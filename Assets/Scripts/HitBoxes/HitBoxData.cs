﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HitBoxData{

    public float damage;
    public float knockbackAngle = 90;
    public float knockbackMagnitude;
    public int priority = 0;
    public float meterGain = 0f;
    public float hitstopTime = 0f;
    public GameObject owner;
    public KnockType knockType = KnockType.Send;

    public enum KnockType
    {
        Twitch = 0, Stun, Send
    };
    /*
    public HitBoxData(float damage, float knockbackAngle, float knockbackMagnitude, int priority, GameObject owner)
    {
        this.damage = damage;
        this.knockbackAngle = knockbackAngle;
        this.knockbackMagnitude = knockbackMagnitude;
        this.priority = priority;
        this.owner = owner;
    }
    
    public HitBoxData(HitBoxData hitBox) // Copy Constructor
    {
        this.damage = hitBox.damage;
        this.knockbackAngle = hitBox.knockbackAngle;
        this.knockbackMagnitude = hitBox.knockbackMagnitude;
        this.priority = hitBox.priority;
        this.owner = hitBox.owner;
    }
    */
}
