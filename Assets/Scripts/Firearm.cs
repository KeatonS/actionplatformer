﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]

public class Firearm : MonoBehaviour {

    public GameObject projectile;
    public float windUpTime;
    public float coolDownTime;
    public float specialCostPerShot = 0;
  
    public bool canStopFiring = false;

    private float lastFireTime = 0;
    private bool result = false;
    private bool shot = false;
    private bool shotFired = false;
    private Transform shootSource;
    private Animator anim;
    private PlayerDynamicStats dynamicStats;
    private PlayerAnimatorParamManager playerParams;
    void Start()
    {
        shootSource = transform.GetChild(0);
        GameObject owner = transform.root.gameObject;
        anim = owner.GetComponent<Animator>();
        dynamicStats = owner.GetComponent<PlayerDynamicStats>();
        playerParams = owner.GetComponent<PlayerAnimatorParamManager>();
    }
    void Update () {

        if (this.isActiveAndEnabled)
        {
            if(dynamicStats.curSpecial >= specialCostPerShot) canStopFiring = FireBulletObject(projectile, true);
        }
	}
    public bool FireBulletObject(GameObject bullet, bool isShooting)
    {
        if (isShooting && !shot)
        {
            StartCoroutine(Delay(windUpTime, coolDownTime, bullet));
            shot = true;
        }
        if (result && shot)
        {
            shotFired = false;
            shot = false;
        }

        return result;
    }
    IEnumerator Delay(float windUp, float coolDown, GameObject bullet)
    {
        result = false;
        yield return new WaitForSeconds(windUp);
        Fire(bullet);
        result = false;
        yield return new WaitForSeconds(coolDown);
        result = true;
    }
    private void Fire(GameObject bullet)
    {
        if (!shotFired)
        {
            GameObject instance = Instantiate(bullet, shootSource.position, shootSource.rotation);
            instance.GetComponent<Projectile>().SetStats(transform.parent.root.gameObject.GetComponent<Rigidbody2D>().velocity, playerParams.GetSideFacing());
            shotFired = true;
            dynamicStats.curSpecial -= specialCostPerShot;
            //playerParams.NotifyOnShoot(specialCostPerShot);
        }
    }

    void OnDisable()
    {
        result = false;
        shot = false;
        shotFired = false;
        canStopFiring = true;
    }
}
