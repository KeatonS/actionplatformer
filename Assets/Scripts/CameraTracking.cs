﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTracking : MonoBehaviour {

    public GameObject player;       //Public variable to store a reference to the player game object
    public float smoothing;     //smoothing rate. Increase for tighter tracking, decrease for slower tracking
    public Vector3 offset;         //Private variable to store the offset distance between the player and camera
    private Rigidbody2D targetRB;
    public float maxSeperation;
    // Use this for initialization
    void Start()
    {
        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        targetRB = player.GetComponent<Rigidbody2D>();
    }

    // LateUpdate is called after Update each frame
    void FixedUpdate()
    {
        //transform.position = Vector3.Lerp(transform.position, player.transform.position + offset + new Vector3(targetRB.velocity.x * velocityMultiplier.x, targetRB.velocity.y * velocityMultiplier.y) , smoothing * Time.deltaTime);
        Vector3 delta = ((player.transform.position + offset) - transform.position);
        delta.z = 0;
        float linearDist = Vector3.Magnitude(delta);
        //float linearDist = Vector3.Distance(player.transform.position, transform.position);
        if (linearDist > maxSeperation)
        {
            delta.Normalize();
            delta *= (linearDist - maxSeperation);
            transform.position += delta;   // no timeDelta in this case
        }
        else
        {
            transform.position += delta * smoothing * Time.deltaTime;
        }
    }
}
