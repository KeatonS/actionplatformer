﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallChecker : MonoBehaviour {

    Rigidbody2D myRB;
    PlayerAnimatorParamManager animParams;
    int groundLayer = 8;

    // Use this for initialization
    void Start()
    {
        myRB = GetComponentInParent<Rigidbody2D>();
        animParams = GetComponentInParent<PlayerAnimatorParamManager>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (myRB.velocity.y <= 0) // Only when velocity is negative allow for change in grounded
        {
            if (col.gameObject.layer == groundLayer)
            {
                animParams.NotifyOnWall(true);
            }
        }
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.layer == groundLayer)
        {
            animParams.NotifyOnWall(true);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.layer == groundLayer)
        {
            animParams.NotifyOnWall(false);
        }
    }
}
