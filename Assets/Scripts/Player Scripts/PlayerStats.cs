﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour{

    [SerializeField]
    private float _maxHealth;
    public float maxHealth
    {
        get { return _maxHealth; }
        set { _maxHealth = value;  GetComponent<PlayerDynamicStats>().UpdateStatUI(); }
    }

    [SerializeField]
    private float _maxSpecial;
    public float maxSpecial
    {
        get { return _maxSpecial; }
        set { _maxSpecial = value; GetComponent<PlayerDynamicStats>().UpdateStatUI(); }
    }

    // Ground Stats
    public float walkSpeed;  
    public float dashSpeed;
    public float groundFriction;
    // Air Control Stats
    public float jumpVelocity;
    public float lowJumpMultiplier;
    public float normalJumpMultiplier;
    public float maxAirSpeedY;
    public float maxAirSpeedX;
    public float airAcceleration;
    // Wall Jumping Stats
    public float wallFriction;
    public float wallJumpVelocityY;
    public float wallJumpVelocityX;
    // Rope Swinging Stats
    public float swingAcceleration;
    public float swingExitVelocity;
    public float swingGravMultiplier;
}
