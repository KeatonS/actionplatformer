﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitHandler : MonoBehaviour {
    
    private List<HitBoxData> hitDataList = new List<HitBoxData>();
    private int highestPrior = 100;
    private Animator playerAnimator;
    private Rigidbody2D playerRB;
    private PlayerDynamicStats dynamicStats;
    private HitBoxData currentBestHit;

    private float knockBackX = 0;
    private float knockBackY = 0;
    private float damage = 0;

    public bool inHitStop = false;

    void Start()
    {
        playerAnimator = GetComponent<Animator>();
        playerRB = GetComponent<Rigidbody2D>();
        dynamicStats = GetComponent<PlayerDynamicStats>();
    }
    public HitBoxData HighestPriorityHit()
    {
        HitBoxData returnHitBox = null;

        foreach (HitBoxData hit in hitDataList)
        { 
            if(hit.priority == 0)// If priority is zero then this is the best hit possible so take the hit
            {
                returnHitBox = hit;
                break;
            }
            else if(hit.priority < highestPrior)
            {
                highestPrior = hit.priority;
                returnHitBox = hit;
            }
        }
        hitDataList.Clear();
        highestPrior = 100;
        return returnHitBox;
    }

    public void AddHitToList(HitBoxData hitData)
    {
        hitDataList.Add(hitData);
    }

    /************************** CUT FROM PLAYER ANIM PARAM MANAGER **********************/
    // Combat Code
    public void NotifyOnHit(HitBoxData hitbox, int side, SpecialAttackProperty special)
    {
        dynamicStats.curSpecial += hitbox.damage / 2;
        if (hitbox.knockType == HitBoxData.KnockType.Send)
        {
            GetComponent<PlayerStateMachineDictionary>().MakeNewDamageState();
            for (int i = 1; i < playerAnimator.layerCount; i++) playerAnimator.SetLayerWeight(i, 0);
            playerAnimator.SetTrigger("TakeDamage");

            StartCoroutine(GetBestHitBox(hitbox, side));
            StartCoroutine(HitStop(hitbox));
            StartCoroutine(KnockBack(hitbox, side));
        }
        else if (hitbox.knockType == HitBoxData.KnockType.Twitch)
        {
            StartCoroutine(FlashOnTwitch());
            dynamicStats.curHealth -= hitbox.damage;
        }

        // Check States.cs -> TakeDamageState to see what is done with these values after they are put in animator
    }
    public void NotifyOnAttack(HitBoxData myData, SpecialAttackProperty special)
    {
        dynamicStats.curSpecial += myData.meterGain;
        StartCoroutine(HitStopForActor(myData, special));
    }

    IEnumerator GetBestHitBox(HitBoxData hitbox, int side)
    {
        this.AddHitToList(hitbox);
        yield return new WaitForFixedUpdate();

        HitBoxData hbox = this.HighestPriorityHit();
        hitbox = hbox;
        currentBestHit = hitbox;
    }
    IEnumerator HitStop(HitBoxData hitbox)
    {

        knockBackX = 0;
        knockBackY = 0;
        damage = 0;
        // Hit Stop
        playerAnimator.SetTrigger("TakeDamage");
        inHitStop = true;
        playerAnimator.speed = 0;
        playerRB.gravityScale = 0;
        playerRB.velocity = Vector2.zero;
        yield return new WaitForSeconds(hitbox.hitstopTime);
        playerAnimator.speed = 1;
        playerRB.gravityScale = 1;
        inHitStop = false;

    }
    IEnumerator KnockBack(HitBoxData hitbox, int side)
    {
        // Knockback
        float a = hitbox.knockbackAngle * Mathf.Deg2Rad;
        Vector2 knockBack = new Vector2(Mathf.Cos(a), Mathf.Sin(a)).normalized * hitbox.knockbackMagnitude;

        knockBackX = knockBack.x * side;
        knockBackY = knockBack.y;
        damage = hitbox.damage;

        dynamicStats.curHealth -= hitbox.damage;
        yield return null;
    }
    IEnumerator HitStopForActor(HitBoxData hitBox, SpecialAttackProperty special)
    {
        GetComponent<PlayerInput>().takeInputs = false;
        playerAnimator.speed = 0;
        playerRB.velocity = Vector2.zero;
        playerRB.gravityScale = 0;
        yield return new WaitForSeconds(hitBox.hitstopTime);
        playerRB.gravityScale = 1;
        playerAnimator.speed = 1;
        GetComponent<PlayerInput>().takeInputs = true;
        if (special != null) special.OnAttack();
    }
    IEnumerator FlashOnTwitch()
    {
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        Color originalColor = renderer.color;
        for (var n = 0; n < 5; n++)
        {
            renderer.material.color = Color.blue;
            yield return new WaitForSeconds(.1f);
            renderer.material.color = originalColor;
            yield return new WaitForSeconds(.1f);
        }
        renderer.material.color = Color.white;

    }

    public float GetDamage() { return damage; }
    public Vector2 GetKnockBack() { return new Vector2(knockBackX, knockBackY); }
    public HitBoxData GetBestHit() { return currentBestHit; }
}
