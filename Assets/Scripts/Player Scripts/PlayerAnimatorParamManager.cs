﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[RequireComponent(typeof(PlayerHitHandler))]

public class PlayerAnimatorParamManager : MonoBehaviour {

    private Animator playerAnimator;
    private Rigidbody2D playerRB;
    //private PlayerDynamicStats dynamicStats;
    private bool againstWall = false;

    // Animator Parameters
    private float horizontalInput = 0;
    private float verticalInput = 0;

    private bool grounded = false;
    private bool wallSlide = false;
    private int sideFacing = 1;
    private bool canRotate = true;
    public bool holdingAttack = false;
    public bool isDead = false;

    void Start()
    {
        playerAnimator = GetComponent<Animator>();
        playerRB = GetComponent<Rigidbody2D> ();
        playerAnimator.SetBool("CanRotate", canRotate);
    }

    void Update()
    {
        horizontalInput = playerAnimator.GetFloat("Horizontal_Input");
        verticalInput = playerAnimator.GetFloat("Vertical_Input");
        holdingAttack = playerAnimator.GetBool("AttackHold");
        // Rotating player Sprite
        RotatePlayer();
        // Current Player Stats
        playerAnimator.SetBool("Grounded", grounded);
        playerAnimator.SetBool("OnWall", wallSlide = ClungToWall());
        playerAnimator.SetBool("AttackLayerActive", playerAnimator.GetLayerWeight(1) == 1);
        playerAnimator.SetFloat("Horizontal_Velocity", playerRB.velocity.x);
        playerAnimator.SetFloat("Vertical_Velocity", playerRB.velocity.y);
    }

    // Private Helper Methods
    private void RotatePlayer()
    {
        if (canRotate && (int)Mathf.Sign(playerAnimator.GetFloat("Horizontal_Input")) == sideFacing * -1 && playerAnimator.GetFloat("Horizontal_Input") != 0) // Side Flipping
        {
            this.gameObject.transform.Rotate(0, -180, 0);
            sideFacing *= -1;
        }
    }
    private bool ClungToWall()
    {
        return againstWall && playerAnimator.GetFloat("Horizontal_Input") != 0 && (Mathf.Sign(playerAnimator.GetFloat("Horizontal_Input")) == sideFacing);//&& stats.sideOfWall == stats.sideFacing;
    }
    // Notifications
    public void NotifyOnGround(bool isOnGround)
    {
        grounded = isOnGround;
    }
    public void NotifyOnWall(bool isOnWall)
    {
        againstWall = isOnWall;
    }
    // Getters
    public bool GetShootInput() { return playerAnimator.GetBool("Shoot"); }
    public bool GetGrounded() { return grounded; }
    public int GetSideFacing() {return sideFacing; }
    // Setters
    public void SetCanRotate(bool value) { this.canRotate = value;}
    public void SetLayerWeight(int layer, int value) { playerAnimator.SetLayerWeight(layer, value); }
    public void SetIsDead(bool value) { if(playerAnimator != null)playerAnimator.SetBool("IsDead", value); isDead = value; }
}
