﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {

    public string playerNumber = "1";
    public bool takeInputs = true;
    public float stickThreshold;
    public float bufferTime = 0.05f;

    private Animator playerAnimator;
    private InputBuffer inputBuffer;
    
    void Start () {
        playerAnimator = GetComponent<Animator>();
        inputBuffer = new InputBuffer(playerAnimator, bufferTime, playerNumber);
    }

    void Update()
    {
        if (takeInputs)
        {
            // Face Button Inputs
            inputBuffer.CheckBuffers(); // Face buttons that aren't true when you hold them down
            playerAnimator.SetBool("JumpHold", Input.GetButton("A_P" + playerNumber));
            playerAnimator.SetBool("AttackHold", Input.GetButton("X_P" + playerNumber));

            playerAnimator.SetBool("Special", Input.GetButton("Shoulder_L_P" + playerNumber));
            playerAnimator.SetBool("Shoot", Input.GetButton("B_P" + playerNumber));

            // Analog Stick / WASD directional inputs
            if (Input.GetAxisRaw("K_Horizontal_P" + playerNumber) != 0) playerAnimator.SetFloat("Horizontal_Input", Input.GetAxisRaw("K_Horizontal_P" + playerNumber)); // Keyboard Input
            else playerAnimator.SetFloat("Horizontal_Input", Input.GetAxisRaw("P_Horizontal_LeftStick_P" + playerNumber)); // Controller Input

            if (Input.GetAxisRaw("K_Vertical_P" + playerNumber) != 0) playerAnimator.SetFloat("Vertical_Input", Input.GetAxisRaw("K_Vertical_P" + playerNumber)); // Keyboard Input
            else playerAnimator.SetFloat("Vertical_Input", Input.GetAxisRaw("P_Vertical_LeftStick_P" + playerNumber)); // Controller Input

            // Digital Markers for Control Stick

            playerAnimator.SetBool("Up", playerAnimator.GetFloat("Vertical_Input") >= stickThreshold);
            playerAnimator.SetBool("Down", playerAnimator.GetFloat("Vertical_Input") <= -stickThreshold);
            playerAnimator.SetBool("Right", playerAnimator.GetFloat("Horizontal_Input") >= stickThreshold);
            playerAnimator.SetBool("Left", playerAnimator.GetFloat("Horizontal_Input") <= -stickThreshold);
        }
        else
        {
            playerAnimator.SetFloat("Horizontal_Input", 0f);
            playerAnimator.SetFloat("Vertical_Input", 0f);
            playerAnimator.SetBool("Jump", false);
            playerAnimator.SetBool("Attack", false);
            playerAnimator.SetBool("Dash", false);
            playerAnimator.SetBool("Special", false);
        }
    }

    //******************* INPUT BUFFERING CODE ************************//
    /*
     * This class is to be used for inputs that only use Input.GetButtonDown.
     * This helps avoid missed inputs since that method only returns true on the first frame. 
     */
    public class InputBuffer
    {

        class Buffer
        {
            float timeAtPress;              // Time when button was first pressed
            public float bufferTime;        // Time the buffer stays true after release
            public string inputName;       // Corresponding Input Code
            public string parameterName;    // Corresponding Animator param
            public Buffer(float bufferTime, string inputName, string parameterName)
            {
                this.timeAtPress = 0;
                this.bufferTime = bufferTime;
                this.inputName = inputName;
                this.parameterName = parameterName;
            }
            public bool GetButtonBuffered(bool buttonPress, float bufferTime)
            {
                bool result = false;
                if (buttonPress == true) { timeAtPress = Time.time; result = true; }                                    // User presses button and it resets the timer
                else if (buttonPress == false && (Time.time - timeAtPress <= bufferTime) && timeAtPress != 0f) result = true;   // User releases button but timer isn't exceed
                return result;                                                                                           // Timer is out and the button is not pressed
            }
        }

        Animator animator;
        Buffer A, B, X, Y, LB, RB, LT, RT;
        List<Buffer> allBuffers;

        public InputBuffer(Animator anim, float bufferTimes, string playerNumber)
        {
            animator = anim;
            allBuffers = new List<Buffer>();
            allBuffers.Add(A = new Buffer(bufferTimes, "A_P" + playerNumber, "Jump"));
            allBuffers.Add(X = new Buffer(bufferTimes*1.5f, "X_P" + playerNumber, "Attack"));
            allBuffers.Add(RB = new Buffer(bufferTimes, "Shoulder_R_P" + playerNumber, "Dash"));
        }

        void SetBuffer(Buffer buff, Animator anim)
        {
            anim.SetBool(buff.parameterName, buff.GetButtonBuffered(Input.GetButtonDown(buff.inputName), buff.bufferTime));
        }

        public void CheckBuffers() // Call this from the input manager
        {
            foreach (Buffer buff in allBuffers)
            {
                SetBuffer(buff, animator);
            }
        }
    }

}
