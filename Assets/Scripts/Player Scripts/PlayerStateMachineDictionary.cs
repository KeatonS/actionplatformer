﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 
     This is a slightly more complex version of a state machine. 
     This uses dictionary and animator state hashes.
     This is relatively faster than using AnimatorStateInfo.IsName()
     This differs from PlayerStateMachineWithStates as it uses the states 
        classes contained in a Dictionary with animatorStateHash as the key
     
*/
[RequireComponent(typeof(PlayerStats))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]

public class PlayerStateMachineDictionary : MonoBehaviour {

    public GameObject firearm;

    private Dictionary<int, State> statesToHash = new Dictionary<int, State>();

    private Rigidbody2D playerRB;
    private Animator playerAnim;
    private PlayerStats playerStats;

    private PlayerAttacks attacks;
    private AnimatorStateInfo animStateInfo;
    private State curState, prevState;
    private int takeDamageHash;

    void Start () {
        playerRB = GetComponent<Rigidbody2D>();
        playerAnim = GetComponent<Animator>();
        playerStats = GetComponent<PlayerStats>();
        attacks = GetComponent<PlayerAttacks>();

        takeDamageHash = Animator.StringToHash("Base Layer." + "TakeDamage");

        InitializeDictionary();

        curState = prevState = new IdleState(gameObject);
    }

    int animStateHash;
    int attackStateHash;
    int shootStateHash;
    int eightWayStateHash;

    int currentHash;

    void FixedUpdate () {

        animStateHash       = playerAnim.GetCurrentAnimatorStateInfo(0).fullPathHash;
        attackStateHash     = playerAnim.GetCurrentAnimatorStateInfo(1).fullPathHash;
        shootStateHash      = playerAnim.GetCurrentAnimatorStateInfo(2).fullPathHash;
        eightWayStateHash   = playerAnim.GetCurrentAnimatorStateInfo(3).fullPathHash;

        if (playerAnim.GetLayerWeight(1) == 1)
            currentHash = (attackStateHash);
        else if (playerAnim.GetLayerWeight(2) == 1)
            currentHash = (shootStateHash);
        else if (playerAnim.GetLayerWeight(3) == 1)
            currentHash = (eightWayStateHash);
        else
            currentHash = (animStateHash);

        try
        {
            SwitchStates(statesToHash[currentHash]);
        }
        catch(KeyNotFoundException e)
        {
            //SwitchStates(statesToHash[animStateHash]);
        }

        curState.Execute();
        ActivateFirearm();
    }

    private void SwitchStates(State newState){
        if (newState != curState)
        {
            Debug.Log(newState.ToString());
            prevState = curState;
            curState = newState;
            prevState.Exit();
            curState.Enter();
        }
    }

    private void ActivateFirearm()
    {
        if (playerAnim.GetLayerWeight(2) == 1)
        {
            firearm.gameObject.SetActive(true);
        }
        else if(firearm.activeSelf)
            firearm.gameObject.SetActive(false);
    }

    public void MakeNewDamageState() // Called in PlayerAnimatorParamManager.cs in NotifyOnHit
    {
        statesToHash[takeDamageHash] = new TakeDamageState(gameObject);
    }

    public void NotifyOnAttackChange()
    {
        InitializeDictionary();
    }

    void InitializeDictionary()
    {
        statesToHash = new Dictionary<int, State>();
        string curLayer = "Base Layer.";
        {
            statesToHash.Add(Animator.StringToHash(curLayer + "Idle"), new IdleState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "Walk"), new WalkState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "Dash"), new DashState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "Jump"), new JumpState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "Fall"), new FallState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "WallSlide"), new WallSlideState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "WallJump"), new WallJumpState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "TakeDamage"), new TakeDamageState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "Die"), new DeathState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "RopeSwing"), new SwingState(gameObject));
        }
        curLayer = "Attack Layer.";
        {
            statesToHash.Add(Animator.StringToHash(curLayer + "Empty"), new StandAttackState(gameObject, attacks.GetAttackFromString("NeutralStandAttack")));
            statesToHash.Add(Animator.StringToHash(curLayer + "CanAttack"), new CanAttackState(gameObject, attacks.GetAttackFromString("NeutralStandAttack")));

            statesToHash.Add(Animator.StringToHash(curLayer + "NeutralStandAttack"), new StandAttackState(gameObject, attacks.GetAttackFromString("NeutralStandAttack")));
            statesToHash.Add(Animator.StringToHash(curLayer + "StandAttack2"), new StandAttackState(gameObject, attacks.GetAttackFromString("StandAttack2")));
            statesToHash.Add(Animator.StringToHash(curLayer + "StandAttack3"), new StandAttackState(gameObject, attacks.GetAttackFromString("StandAttack3")));
            statesToHash.Add(Animator.StringToHash(curLayer + "UpStandAttack"), new StandAttackState(gameObject, attacks.GetAttackFromString("UpStandAttack")));
            statesToHash.Add(Animator.StringToHash(curLayer + "SideStandAttack"), new StandAttackState(gameObject, attacks.GetAttackFromString("SideStandAttack")));
            statesToHash.Add(Animator.StringToHash(curLayer + "DownStandAttack"), new StandAttackState(gameObject, attacks.GetAttackFromString("DownStandAttack")));

            statesToHash.Add(Animator.StringToHash(curLayer + "NeutralAirAttack"), new AirAttackState(gameObject, attacks.GetAttackFromString("NeutralAirAttack")));
            statesToHash.Add(Animator.StringToHash(curLayer + "SideAirAttack"), new AirAttackState(gameObject, attacks.GetAttackFromString("SideAirAttack")));
            statesToHash.Add(Animator.StringToHash(curLayer + "UpAirAttack"), new AirAttackState(gameObject, attacks.GetAttackFromString("UpAirAttack")));
            statesToHash.Add(Animator.StringToHash(curLayer + "DownAirAttack"), new AirAttackState(gameObject, attacks.GetAttackFromString("DownAirAttack")));

            statesToHash.Add(Animator.StringToHash(curLayer + "DashAttack"), new DashAttackState(gameObject, attacks.GetAttackFromString("NeutralStandAttack")));
        }
        curLayer = "Shoot Layer.";
        {
            statesToHash.Add(Animator.StringToHash(curLayer + "Idle"), new StandShootState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "Walk"), new WalkShootState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "Dash"), new DashShootState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "Jump"), new JumpShootState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "Fall"), new FallShootState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "WallSlide"), new WallSlideShootState(gameObject));
        }
        curLayer = "8 Layer Copy.";
        {
            statesToHash.Add(Animator.StringToHash(curLayer + "Empty"), new State(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "8Way_Stand_Reelback"), new AimState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "8Way_Air_Reelback"), new AimState(gameObject));

            statesToHash.Add(Animator.StringToHash(curLayer + "8Way_Stand_StraightUp"), new EightWayStandShootState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "8Way_Stand_Up"), new EightWayStandShootState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "8Way_Stand_Neutral"), new EightWayStandShootState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "8Way_Stand_Down"), new EightWayStandShootState(gameObject));

            statesToHash.Add(Animator.StringToHash(curLayer + "8Way_Air_StraightUp"), new EightWayAirShootState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "8Way_Air_Up"), new EightWayAirShootState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "8Way_Air_Neutral"), new EightWayAirShootState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "8Way_Air_Down"), new EightWayAirShootState(gameObject));
            statesToHash.Add(Animator.StringToHash(curLayer + "8Way_Air_StraightDown"), new EightWayAirShootState(gameObject));

        }

    }
}
