﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerAttackData : MonoBehaviour {

    public List<HitBoxData> hitDataList;

}
